**Diamond Scheduler App** is built with React using Typescript. This is an app that makes it possible for the users to create an account i.e signup before using the To-do List Application itself. Signed up users can write a list of the task they intend to do. The option of marking a particular task has completed is also given to users. Lastly, users can also remove tasks when they decide to do so. 

**How to use the applicaton features**

    - Authentication Features

        1. SignUp with your full name, email address and a unique password.
        2. SignIn with your email address and password (this only possible if users have signedup earlier ) 
        3. Forgot Password: should a user forget the current password used, the user can provide the email address used during the signup process to reset the password.
        4. Reset Password: Should the user decide to change the current password for reasons best known to them, the user will have to provide the new password and confirm it.
        5. Sign Out: A user has the liberty of signing out of the application at any time and this can be done be clicking the SIGNOUT button.

    - The functionality of the app
        1. Update list of To-do's: Users can update their list of tasks to be carried out.
        2. Mark completed tasks: After a task has been completed, users can indicate that the task has been  completed by clicking on the "complete button". 
        3. Remove tasks: Users can remove any task they want to, whether it is completed or not.


**To clone the project**

`https://gitlab.com/MartianTolu/complete-app.git`

After cloning, cd into directory/folder

`$ cd complete-app`

**Setup**
You will need to install some packages, if they are not yet installed on your machine:

- Node.js (v10.0.0 or higher; LTS)
- optionally, you can install typescript globally.

`$ yarn global add typescript`

**NOTE**: your typescript version should equivalent or greater than 3.8.3_

**Installation**

Install project dependencies by running the command below in the project directory:
`$ yarn`

**Running The App**

It's now ready to launch.
First run the command `npm test` on your terminal and see if everything is all good. Then run:

`yarn start`

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

