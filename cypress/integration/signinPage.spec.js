describe("Test the Landing page", () => {
    before(() => {
        // visit the index (signin) page
        cy.visit('/signin');
    });

    it("should successfully load the signin page", () => {
        cy.get('h1').should('contain', 'SignIn');
    })

    it("should contain 2 input fields and a single button in pristine state", () => {

        cy.get('form').find('Input').first().should('have.attr', 'type', 'text');
        cy.get('form').find('Input').first().should('have.attr', 'name', 'email');
        cy.get('form').find('Input').first().should('be.empty');

        cy.get('form').find('Input').last().should('have.attr', 'type', 'password');
        cy.get('form').find('Input').last().should('be.empty');

        cy.get('form').find('Button').first().should('have.length', 1);
        cy.get('form').find('Button').first().should('have.attr', 'type', 'submit');
        cy.get('form').find('Button').first().should('contain', 'Sign In');
        cy.get('form').find('Button').first().should('be.visible');
    })
    it("should not redirect when the fields validation fails", () => {
        cy.get('form').find('Input').first().focus().type('hello@gmaill'); // not a valid email
        cy.get('form').find('Input').last().focus().type('a35%OP'); // password less than 7 chars
        cy.get('form').find('Button').contains('Sign In').click();
        cy.wait(500); //wait
        cy.url().should('include', '/signin');
    });

    it("should locate the forgot password link", () => {
        cy.contains('Forgot Password?');
    });
    it("should locate the Don't have an account question tag", () => {
        cy.get('p').should('contain', 'Don\'t have an account?');
    });
    it("should locate the sign up link", () => {
        cy.contains('Sign Up');
    });
});