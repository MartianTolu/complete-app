describe("Test the Home page", () => {
    before(() => {
        cy.login();
        cy.wait(1200); // wait 1.2 second 
        cy.visit('/home');
    });

    it('should visit the home page', () => {
        cy.wait(800);
        cy.url().should('include', '/home');
    });
    it("should mark the to-do list has complete", () => {
        cy.get('button').should('have.attr', 'type', 'button');
        cy.get('button').contains('Complete').click();
    })
    it("should remove complete todo item", () => {
        cy.get('button').should('have.attr', 'type', 'button');
        cy.get('button').contains('x').click();
    })
});