describe("Test the Landing page", () => {
    before(() => {
        // visit the index (landing) page
        cy.visit('/');
    });
    it("should locate the title of the app", () => {
        cy.get('h1').should('contain', 'Diamond Scheduler App');
    })
    it("should locate the catch-phrase of the app", () => {
        cy.get('p').should('contain', '...no job too small');
    })
    it("should locate the signin link", () => {
        cy.contains('Sign In').click();
        cy.wait(1200); // wait 1.2 second 
        cy.url().should('include', '/signin');
    })
});