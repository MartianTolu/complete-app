describe("Test the signup page", () => {
    before(() => {
        // visit the index (signin) page
        cy.visit('/signup');
    });

    it("should successfully load the signin page", () => {
        cy.get('h1').should('contain', 'SignUp');
    })

    it("should contain 2 input fields and a single button in pristine state", () => {

        cy.get('form').find('Input').eq(0).should('have.attr', 'type', 'text');
        cy.get('form').find('Input').eq(0).should('be.empty');

        cy.get('form').find('Input').eq(1).should('have.attr', 'name', 'email');
        cy.get('form').find('Input').eq(1).should('be.empty');

        cy.get('form').find('Input').eq(2).should('have.attr', 'name', 'passwordOne');
        cy.get('form').find('Input').eq(2).should('be.empty');

        cy.get('form').find('Input').eq(3).should('have.attr', 'name', 'passwordTwo');
        cy.get('form').find('Input').eq(3).should('be.empty');

        cy.get('form').find('Button').first().should('have.length', 1);
        cy.get('form').find('Button').first().should('have.attr', 'type', 'submit');
        cy.get('form').find('Button').first().should('contain', 'Sign Up');
        cy.get('form').find('Button').first().should('be.visible');
    })
});