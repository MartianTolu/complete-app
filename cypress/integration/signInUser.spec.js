describe("Test the signInUser logic", () => {
    before(() => {
        // visit the index (signin) page
        cy.visit('/signin');
    });

    it("should successfully load the signin page", () => {
        cy.get('h1').should('contain', 'SignIn');
    })

    it("should login user with correct login details", () => {
        cy.get('form').find('Input').first().focus().type('demisemi@gmail.com'); // not a valid email
        cy.get('form').find('Input').last().focus().type('111demi'); // password less than 7 chars
        cy.get('form').find('Button').contains('Sign In').click();
        cy.wait(500); //wait
        cy.url().should('include', '/home');
    });

});