describe("Test the signUpUser logic", () => {
    before(() => {
        // visit the index (signin) page
        cy.visit('/signup');
    });

    it("should successfully load the signin page", () => {
        cy.get('h1').should('contain', 'SignUp');
    })

    it("should login user with correct login details", () => {
        cy.get('form').find('Input').eq(0).focus().type('Test Bot');
        cy.get('form').find('Input').eq(1).focus().type('testing@gmail.com');
        cy.get('form').find('Input').eq(2).focus().type('111demi');
        cy.get('form').find('Input').eq(3).focus().type('111demi');
        cy.get('form').find('Button').contains('Sign Up').click({ force: true });
        cy.wait(500); //wait
        cy.url().should('include', '/home');
    });
});