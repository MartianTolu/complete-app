describe("Test the Home page", () => {
    before(() => {
        cy.login();
        cy.wait(1200); // wait 1.2 second 
        cy.visit('/home');
    });

    it('should visit the home page', () => {
        cy.wait(800);
        cy.url().should('include', '/home');
    });

    it("should successfully load the home page", () => {
        cy.get('h1').should('contain', 'Welcome!');
    })

});