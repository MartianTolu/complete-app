import React from 'react';

const App = () => (
    <div className="app">
        <h1>Diamond Scheduler App</h1>
        <p>...no job too small</p>
    </div>
);

export default App;