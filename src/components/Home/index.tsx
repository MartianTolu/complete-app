import React, { useState } from "react";
import "../App.css";
import { withAuthorization } from '../Session';



function Todo({ todo, index, completeTodo, removeTodo }) {
    return (
        <div
            id="todo"
            className="todo"
            style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
        >
            {todo.text}

            <div>
                <button className="complete" onClick={() => completeTodo(index)}>Complete</button>
                <button className="remove" onClick={() => removeTodo(index)}>x</button>
            </div>
        </div>
    );
}

function TodoForm({ addTodo }) {
    const [value, setValue] = useState("");

    const handleSubmit = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        if (!value) return;
        addTodo(value);
        setValue("");
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                className="input"
                value={value}
                onChange={e => setValue(e.target.value)}
            />
        </form>
    );
}

function HomePage() {
    const [todos, setTodos] = useState([
        {
            text: "Learn about React",
            isCompleted: false
        },
        {
            text: "Meet friend for lunch",
            isCompleted: false
        },
        {
            text: "Build really cool todo app",
            isCompleted: true
        }
    ]);

    const addTodo = (text: string, isCompleted: boolean) => {
        const newTodos = [...todos, { text, isCompleted }];
        setTodos(newTodos);
    };

    const completeTodo = (index: number) => {
        const newTodos = [...todos];
        newTodos[index].isCompleted = true;
        setTodos(newTodos);
    };

    const removeTodo = (index: number) => {
        const newTodos = [...todos];
        newTodos.splice(index, 1);
        setTodos(newTodos);
    };

    return (
        <div className="app">
            <h1>Welcome!</h1>
            <p>Subtracting from your list of priorities is as important as adding to it...</p>
            <div className="todo-list">
                {todos.map((todo, index) => (
                    <Todo
                        key={index}
                        index={index}
                        todo={todo}
                        completeTodo={completeTodo}
                        removeTodo={removeTodo}
                    />
                ))}
                <TodoForm addTodo={addTodo} />
            </div>
        </div>
    );
}

const condition = authUser => !!authUser;

export default withAuthorization(condition)(HomePage);