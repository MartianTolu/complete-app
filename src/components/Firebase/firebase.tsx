import * as firebase from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAiulp2VTt2adNcIlg5jSSHtWuZuW0kbsI",
    authDomain: "complete-todo-app-20f15.firebaseapp.com",
    databaseURL: "https://complete-todo-app-20f15.firebaseio.com",
    projectId: "complete-todo-app-20f15",
    storageBucket: "complete-todo-app-20f15.appspot.com",
    messagingSenderId: "1077683944865",
    appId: "1:1077683944865:web:e5bf50055fdc576a9b1fe0"
};

class Firebase {
    app: firebase.app.App
    auth: firebase.auth.Auth
    static doCreateUserWithEmailAndPassword: Function;
    static doSignInWithEmailAndPassword: Function;

    constructor() {
        this.app = firebase.initializeApp(config);
        this.auth = firebase.auth(this.app);
    }

    // *** Auth API ***

    public doCreateUserWithEmailAndPassword = (email: string, password: string) =>
        this.auth.createUserWithEmailAndPassword(email, password);


    doSignInWithEmailAndPassword = (email: string, password: string) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = (email: string) => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = (password: string) =>
        this.auth.currentUser.updatePassword(password);
}

export default Firebase;
